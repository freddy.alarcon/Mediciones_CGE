from pythonping import ping
import datetime
import subprocess
import time
import os
import requests
import json
import pyautogui

Rutabase = os.getcwd()
Imagenes = 'Imagenes'
Rutaimagenes = os.path.join(Rutabase, Imagenes)

#definir parametros para realizar la consulta y convertir esta consulta a una lista
url = 'https://linux.equanschile.cl/api_medidores_sonel/api.php'
consulta = {'id': 'zQ3@6f[vg]{c5wmdb=ra3STKPxVHBJ', 'tarea': 'Consultar'}
x = requests.post(url, data=consulta)
IPs = x.json()
num = 1

#Conectamos la VPN
time.sleep(5)
info = subprocess.STARTUPINFO()
info.dwFlags = 1
info.wShowWindow = 1
subprocess.Popen("C:\Program Files\Fortinet\FortiClient\FortiClientConsole.exe", startupinfo=info)
time.sleep(10)
pyautogui.press('tab', presses=6, interval=1)
pyautogui.press('enter')
time.sleep(60)

#Acepta el certificado si es que sale
if pyautogui.locateOnScreen(os.path.join(Rutaimagenes, 'Aviso.png')) != None:
	pyautogui.press('left')
	time.sleep(2)
	pyautogui.press('enter')
	time.sleep(30)

time.sleep(10)

#itera sobre las ip de la lista para revisar sus estado
for ip in IPs:

    print(f'{num}. IP: {ip["IP"]}')

    for x in range(1,5):
        if ping(ip['IP'], verbose=True, count=1).success() == True:
            conexion = 'Conectado'
            break
        else:
            conexion = 'Desconectado'
            if x == 4: break
        
    print('--------')

    inserta = {'id': 'zQ3@6f[vg]{c5wmdb=ra3STKPxVHBJ', 'tarea': 'Insertar', 'ip': ip['IP'], 'estado': conexion}
    requests.post(url, data=inserta)

    num += 1


#Desconectamos la VPN
subprocess.Popen("C:\Program Files\Fortinet\FortiClient\FortiClientConsole.exe", startupinfo=info)
time.sleep(5)
pyautogui.press('tab')
time.sleep(2)
pyautogui.press('enter')

time.sleep(30)
print('Termine')